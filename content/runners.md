## Runners

- A runner is an isolated (virtual) machine that picks up builds through the
coordinator API of GitLab CI.

- You can create as many Runners as you need on different machines

- The Runners screen

- Specific vs Shared Runners

- Installation vs registration

- Tagged Runners
